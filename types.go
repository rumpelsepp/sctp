package sctp

type SCTPStatus struct {
	AssocID            int32
	State              int32
	RWND               int32
	Unackdata          uint16
	Penddata           uint16
	Instrms            uint16
	Outstrms           uint16
	FragmentationPoint uint32
}

type SCTPPeerInfo struct {
	AssocID int32
	_       uintptr // TODO: How large is this?
	State   int32
	CWND    uint32
	SRTT    uint32
	RTO     uint32
	MTU     uint32
}

const (
	SCTP_UNORDERED = 1 << iota
	SCTP_ADDR_OVER
	SCTP_ABORT
	SCTP_SACK_IMMEDIATELY
	SCTP_EOF
)

const (
	SOL_SCTP = 132
)

// The following symbols come from the Sockets API Extensions for
// SCTP <draft-ietf-tsvwg-sctpsocket-07.txt>.

const (
	SCTP_RTOINFO                = 0
	SCTP_ASSOCINFO              = 1
	SCTP_INITMSG                = 2
	SCTP_NODELAY                = 3 // Get/set nodelay option.
	SCTP_AUTOCLOSE              = 4
	SCTP_SET_PEER_PRIMARY_ADDR  = 5
	SCTP_PRIMARY_ADDR           = 6
	SCTP_ADAPTATION_LAYER       = 7
	SCTP_DISABLE_FRAGMENTS      = 8
	SCTP_PEER_ADDR_PARAMS       = 9
	SCTP_DEFAULT_SEND_PARAM     = 10
	SCTP_EVENTS                 = 11
	SCTP_I_WANT_MAPPED_V4_ADDR  = 12 // Turn on/off mapped v4 addresses
	SCTP_MAXSEG                 = 13 // Get/set maximum fragment.
	SCTP_STATUS                 = 14
	SCTP_GET_PEER_ADDR_INFO     = 15
	SCTP_DELAYED_ACK_TIME       = 16
	SCTP_DELAYED_ACK            = SCTP_DELAYED_ACK_TIME
	SCTP_DELAYED_SACK           = SCTP_DELAYED_ACK_TIME
	SCTP_CONTEXT                = 17
	SCTP_FRAGMENT_INTERLEAVE    = 18
	SCTP_PARTIAL_DELIVERY_POINT = 19 // Set/Get partial delivery point
	SCTP_MAX_BURST              = 20 // Set/Get max burst
	SCTP_AUTH_CHUNK             = 21 // Set only: add a chunk type to authenticate
	SCTP_HMAC_IDENT             = 22
	SCTP_AUTH_KEY               = 23
	SCTP_AUTH_ACTIVE_KEY        = 24
	SCTP_AUTH_DELETE_KEY        = 25
	SCTP_PEER_AUTH_CHUNKS       = 26 // Read only
	SCTP_LOCAL_AUTH_CHUNKS      = 27 // Read only
	SCTP_GET_ASSOC_NUMBER       = 28 // Read only
)
