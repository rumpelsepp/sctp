package sctp

import (
	"net"
	"strconv"
	"time"

	"golang.org/x/sys/unix"
)

func roundDurationUp(d time.Duration, to time.Duration) time.Duration {
	return (d + to - 1) / to
}

func boolint(v bool) int {
	if v {
		return 1
	}
	return 0
}

// ipEmptyString is like ip.String except that it returns
// an empty string when ip is unset.
func ipEmptyString(ip net.IP) string {
	if len(ip) == 0 {
		return ""
	}
	return ip.String()
}

func sockaddrToSCTP(sa unix.Sockaddr) net.Addr {
	switch sa := sa.(type) {
	case *unix.SockaddrInet4:
		return &SCTPAddr{IP: sa.Addr[0:], Port: sa.Port}
	case *unix.SockaddrInet6:
		return &SCTPAddr{IP: sa.Addr[0:], Port: sa.Port, Zone: strconv.Itoa(int(sa.ZoneId))}
	}
	return nil
}
