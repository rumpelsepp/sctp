package sctp

import (
	"bytes"
	"context"
	"encoding/binary"
	"net"
	"os"
	"runtime"
	"strconv"
	"time"
	"unsafe"

	"golang.org/x/sys/unix"
)

func getsockopt(fd int, optname int, optval unsafe.Pointer, optlen *uint32) (uintptr, uintptr, error) {
	r0, r1, errno := unix.Syscall6(
		unix.SYS_GETSOCKOPT,
		uintptr(fd),
		SOL_SCTP,
		uintptr(optname),
		uintptr(optval),
		uintptr(unsafe.Pointer(optlen)),
		0)
	if errno != 0 {
		return r0, r1, errno
	}
	return r0, r1, nil
}

func setKeepAlive(fd int, keepalive bool) error {
	err := unix.SetsockoptInt(fd, unix.SOL_SOCKET, unix.SO_KEEPALIVE, boolint(keepalive))
	if err != nil {
		return os.NewSyscallError("setsockopt", err)
	}
	return nil
}

func setKeepAlivePeriod(fd int, d time.Duration) error {
	// The kernel expects seconds so round to next highest second.
	secs := int(roundDurationUp(d, time.Second))
	if err := unix.SetsockoptInt(fd, unix.IPPROTO_TCP, unix.TCP_KEEPINTVL, secs); err != nil {
		return os.NewSyscallError("setsockopt", err)
	}
	err := unix.SetsockoptInt(fd, unix.IPPROTO_TCP, unix.TCP_KEEPIDLE, secs)
	if err != nil {
		return os.NewSyscallError("setsockopt", err)
	}
	return nil
}

func setLinger(fd int, sec int) error {
	var l unix.Linger
	if sec >= 0 {
		l.Onoff = 1
		l.Linger = int32(sec)
	} else {
		l.Onoff = 0
		l.Linger = 0
	}
	err := unix.SetsockoptLinger(fd, unix.SOL_SOCKET, unix.SO_LINGER, &l)
	if err != nil {
		return os.NewSyscallError("setsockopt", err)
	}
	return nil
}

func setNoDelay(fd int, noDelay bool) error {
	err := unix.SetsockoptInt(fd, unix.IPPROTO_TCP, unix.TCP_NODELAY, boolint(noDelay))
	runtime.KeepAlive(fd)
	return os.NewSyscallError("setsockopt", err)
}

func newSCTPConn(fd int) *SCTPConn {
	c := &SCTPConn{
		fd:   fd,
		file: os.NewFile(uintptr(fd), "sctp-conn"),
	}
	return c
}

func (a *SCTPAddr) family() int {
	if a == nil || len(a.IP) <= net.IPv4len {
		return unix.AF_INET
	}
	if a.IP.To4() != nil {
		return unix.AF_INET
	}
	return unix.AF_INET6
}

func (a *SCTPAddr) sockaddr() (unix.Sockaddr, error) {
	switch a.family() {
	case unix.AF_INET:
		var ip [4]byte
		copy(ip[:], a.IP[12:])
		return &unix.SockaddrInet4{Addr: ip, Port: a.Port}, nil
	case unix.AF_INET6:
		var (
			ip  [16]byte
			z   int
			err error
		)
		z, err = strconv.Atoi(a.Zone)
		if err != nil {
			return nil, err
		}
		copy(ip[:], a.IP[:16])
		return &unix.SockaddrInet6{Addr: ip, Port: a.Port, ZoneId: uint32(z)}, nil
	}
	panic("BUG: address is invalid")
}

func dialSCTP(raddr *SCTPAddr) (*SCTPConn, error) {
	fd, err := unix.Socket(unix.AF_INET, unix.SOCK_STREAM, unix.IPPROTO_SCTP)
	if err != nil {
		return nil, err
	}
	addr, err := raddr.sockaddr()
	if err != nil {
		return nil, err
	}
	if err := unix.Connect(fd, addr); err != nil {
		return nil, err
	}
	if err := unix.SetNonblock(fd, true); err != nil {
		return nil, err
	}
	return newSCTPConn(fd), nil
}

func (c *SCTPConn) getStatus() (*SCTPStatus, error) {
	var (
		status SCTPStatus
		size   uint32 = 1024 // FIXME: do not hardcode len
		buf    [1024]byte
	)
	_, _, err := getsockopt(
		c.fd,
		SCTP_STATUS,
		unsafe.Pointer(&buf[0]),
		&size,
	)
	if err != nil {
		return nil, os.NewSyscallError("getsockopt", err)
	}
	if err := binary.Read(bytes.NewReader(buf[:]), binary.LittleEndian, &status); err != nil {
		return nil, err
	}
	return &status, nil
}

func (ln *SCTPListener) ok() bool {
	return ln != nil && ln.file != nil
}

func (ln *SCTPListener) accept() (*SCTPConn, error) {
	fd, _, err := unix.Accept(ln.fd)
	if err != nil {
		return nil, err
	}
	if err := unix.SetNonblock(fd, true); err != nil {
		return nil, err
	}
	return newSCTPConn(fd), nil
}

func (ln *SCTPListener) close() error {
	return ln.file.Close()
}

func listenSCTP(ctx context.Context, network string, laddr *SCTPAddr) (*SCTPListener, error) {
	fd, err := unix.Socket(unix.AF_INET, unix.SOCK_STREAM, unix.IPPROTO_SCTP)
	if err != nil {
		return nil, err
	}
	addr, err := laddr.sockaddr()
	if err != nil {
		return nil, err
	}
	if err := unix.Bind(fd, addr); err != nil {
		return nil, err
	}
	if err := unix.Listen(fd, 128); err != nil {
		return nil, err
	}
	return &SCTPListener{
		fd:    fd,
		file:  os.NewFile(uintptr(fd), "sctp-listener"),
		laddr: laddr,
	}, nil
}
