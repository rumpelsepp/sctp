package sctp

import (
	"context"
	"errors"
	"io"
	"net"
	"os"
	"strconv"
	"syscall"
	"time"
)

// SCTPAddr represents the address of a SCTP end point.
type SCTPAddr struct {
	IP   net.IP
	Port int
	Zone string // IPv6 scoped addressing zone
}

// Network returns the address's network name, "sctp".
func (a *SCTPAddr) Network() string {
	return "sctp"
}

func (a *SCTPAddr) String() string {
	if a == nil {
		return "<nil>"
	}
	ip := ipEmptyString(a.IP)
	if a.Zone != "" {
		return net.JoinHostPort(ip+"%"+a.Zone, strconv.Itoa(a.Port))
	}
	return net.JoinHostPort(ip, strconv.Itoa(a.Port))
}

func (a *SCTPAddr) isWildcard() bool {
	if a == nil || a.IP == nil {
		return true
	}
	return a.IP.IsUnspecified()
}

func (a *SCTPAddr) opAddr() net.Addr {
	if a == nil {
		return nil
	}
	return a
}

// ResolveSCTPAddr parses addr as a SCTP address of the form "host:port"
// or "[ipv6-host%zone]:port" and resolves a pair of domain name and
// port name on the network net, which must be "sctp", "sctp4" or
// "sctp6".  A literal address or host name for IPv6 must be enclosed
// in square brackets, as in "[::1]:80", "[ipv6-host]:http" or
// "[ipv6-host%zone]:80".
func ResolveSCTPAddr(network, address string) (*SCTPAddr, error) {
	switch network {
	case "sctp", "sctp4", "sctp6":
	default:
		return nil, net.UnknownNetworkError(network)
	}
	addr, err := net.ResolveTCPAddr("tcp", address)
	if err != nil {
		return nil, err
	}
	return &SCTPAddr{IP: addr.IP, Port: addr.Port, Zone: addr.Zone}, nil
}

// SCTPConn is an implementation of the Conn interface for SCTP network
// connections.
type SCTPConn struct {
	net.Conn
	laddr net.Addr
	raddr net.Addr
	net   string
	fd    int
	file  *os.File
}

func (c *SCTPConn) ok() bool {
	return c != nil && c.file != nil
}

func (c *SCTPConn) Read(p []byte) (int, error) {
	if !c.ok() {
		return 0, syscall.EINVAL
	}
	n, err := c.file.Read(p)
	if err != nil && err != io.EOF {
		err = &net.OpError{Op: "read", Net: c.net, Source: c.laddr, Addr: c.raddr, Err: err}
	}
	return n, err
}

// ReadFrom implements the io.ReaderFrom ReadFrom method.
func (c *SCTPConn) ReadFrom(r io.Reader) (int64, error) {
	if !c.ok() {
		return 0, syscall.EINVAL
	}
	n, err := c.file.ReadFrom(r)
	if err != nil && err != io.EOF {
		err = &net.OpError{Op: "read", Net: c.net, Source: c.laddr, Addr: c.raddr, Err: err}
	}
	return n, err
}

func (c *SCTPConn) Write(p []byte) (int, error) {
	if !c.ok() {
		return 0, syscall.EINVAL
	}
	n, err := c.file.Write(p)
	if err != nil && err != io.EOF {
		err = &net.OpError{Op: "write", Net: c.net, Source: c.laddr, Addr: c.raddr, Err: err}
	}
	return n, err
}

func (c *SCTPConn) Close() error {
	if !c.ok() {
		return syscall.EINVAL
	}
	return c.file.Close()
}

func (c *SCTPConn) SetLinger(sec int) error {
	if !c.ok() {
		return syscall.EINVAL
	}
	if err := setLinger(c.fd, sec); err != nil {
		return &net.OpError{Op: "set", Net: c.net, Source: c.laddr, Addr: c.raddr, Err: err}
	}
	return nil
}

func (c *SCTPConn) SetKeepAlive(keepalive bool) error {
	if !c.ok() {
		return syscall.EINVAL
	}
	if err := setKeepAlive(c.fd, keepalive); err != nil {
		return &net.OpError{Op: "set", Net: c.net, Source: c.laddr, Addr: c.raddr, Err: err}
	}
	return nil
}

func (c *SCTPConn) SetKeepAlivePeriod(d time.Duration) error {
	if !c.ok() {
		return syscall.EINVAL
	}
	if err := setKeepAlivePeriod(c.fd, d); err != nil {
		return &net.OpError{Op: "set", Net: c.net, Source: c.laddr, Addr: c.raddr, Err: err}
	}
	return nil
}

func (c *SCTPConn) SetNoDelay(noDelay bool) error {
	if !c.ok() {
		return syscall.EINVAL
	}
	if err := setNoDelay(c.fd, noDelay); err != nil {
		return &net.OpError{Op: "set", Net: c.net, Source: c.laddr, Addr: c.raddr, Err: err}
	}
	return nil
}

func (c *SCTPConn) GetStatus() (*SCTPStatus, error) {
	if !c.ok() {
		return nil, syscall.EINVAL
	}
	status, err := c.getStatus()
	if err != nil {
		return nil, &net.OpError{Op: "get", Net: c.net, Source: c.laddr, Addr: c.raddr, Err: err}
	}
	return status, nil
}

// DialSCTP connects to the remote address raddr on the network net,
// which must be "sctp", "sctp4", or "sctp6".  If laddr is not nil, it is
// used as the local address for the connection.
func DialSCTP(network string, laddr, raddr *SCTPAddr) (*SCTPConn, error) {
	switch network {
	case "sctp", "sctp4", "sctp6":
	default:
		return nil, &net.OpError{Op: "dial", Net: network, Source: laddr.opAddr(), Addr: raddr.opAddr(), Err: net.UnknownNetworkError(network)}
	}
	if raddr == nil {
		return nil, &net.OpError{Op: "dial", Net: network, Source: laddr.opAddr(), Addr: nil, Err: errors.New("missing address")}
	}
	c, err := dialSCTP(raddr)
	if err != nil {
		return nil, &net.OpError{Op: "dial", Net: network, Source: laddr.opAddr(), Addr: raddr.opAddr(), Err: err}
	}
	return c, nil
}

// SCTPListener is a SCTP network listener. Clients should typically
// use variables of type Listener instead of assuming SCTP.
type SCTPListener struct {
	fd    int
	file  *os.File
	net   string
	laddr net.Addr
}

// AcceptSCTP accepts the next incoming call and returns the new
// connection.
func (l *SCTPListener) AcceptSCTP() (*SCTPConn, error) {
	if !l.ok() {
		return nil, syscall.EINVAL
	}
	c, err := l.accept()
	if err != nil {
		return nil, &net.OpError{Op: "accept", Net: l.net, Source: nil, Addr: l.laddr, Err: err}
	}
	return c, nil
}

// Accept implements the Accept method in the Listener interface; it
// waits for the next call and returns a generic Conn.
func (l *SCTPListener) Accept() (net.Conn, error) {
	if !l.ok() {
		return nil, syscall.EINVAL
	}
	c, err := l.accept()
	if err != nil {
		return nil, &net.OpError{Op: "accept", Net: l.net, Source: nil, Addr: l.laddr, Err: err}
	}
	return c, nil
}

// Close stops listening on the SCTP address.
// Already Accepted connections are not closed.
func (l *SCTPListener) Close() error {
	if !l.ok() {
		return syscall.EINVAL
	}
	if err := l.close(); err != nil {
		return &net.OpError{Op: "close", Net: l.net, Source: nil, Addr: l.laddr, Err: err}
	}
	return nil
}

// Addr returns the listener's network address, a *SCTPAddr.
// The Addr returned is shared by all invocations of Addr, so
// do not modify it.
func (l *SCTPListener) Addr() net.Addr { return l.laddr }

// SetDeadline sets the deadline associated with the listener.
// A zero time value disables the deadline.
func (l *SCTPListener) SetDeadline(t time.Time) error {
	if !l.ok() {
		return syscall.EINVAL
	}
	if err := l.file.SetDeadline(t); err != nil {
		return &net.OpError{Op: "set", Net: l.net, Source: nil, Addr: l.laddr, Err: err}
	}
	return nil
}

// File returns a copy of the underlying os.File, set to blocking
// mode. It is the caller's responsibility to close f when finished.
// Closing l does not affect f, and closing f does not affect l.
//
// The returned os.File's file descriptor is different from the
// connection's. Attempting to change properties of the original
// using this duplicate may or may not have the desired effect.
func (l *SCTPListener) File() (*os.File, error) {
	if !l.ok() {
		return nil, syscall.EINVAL
	}
	return l.file, nil
}

// ListenSCTP announces on the SCTP address laddr and returns a SCTP
// listener. Net must be "sctp", "sctp4", or "sctp6".  If laddr has a
// port of 0, ListenSCTP will choose an available port. The caller can
// use the Addr method of SCTPListener to retrieve the chosen address.
func ListenSCTP(network string, laddr *SCTPAddr) (*SCTPListener, error) {
	switch network {
	case "sctp", "sctp4", "sctp6":
	default:
		return nil, &net.OpError{Op: "listen", Net: network, Source: nil, Addr: laddr.opAddr(), Err: net.UnknownNetworkError(network)}
	}
	if laddr == nil {
		laddr = &SCTPAddr{}
	}
	ln, err := listenSCTP(context.Background(), network, laddr)
	if err != nil {
		return nil, &net.OpError{Op: "listen", Net: network, Source: nil, Addr: laddr.opAddr(), Err: err}
	}
	return ln, nil
}
