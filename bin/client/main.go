package main

import "git.sr.ht/~rumpelsepp/sctp"

func main() {
	addr, err := sctp.ResolveSCTPAddr("sctp", "localhost:1234")
	if err != nil {
		panic(err)
	}
	conn, err := sctp.DialSCTP("sctp", nil, addr)
	if err != nil {
		panic(err)
	}
	conn.Write([]byte("Dere World!\n"))
	conn.Close()
}
