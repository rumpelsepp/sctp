package main

import (
	"io"
	"os"

	"git.sr.ht/~rumpelsepp/sctp"
)

func main() {
	addr, err := sctp.ResolveSCTPAddr("sctp", "localhost:1234")
	if err != nil {
		panic(err)
	}
	ln, err := sctp.ListenSCTP("sctp", addr)
	if err != nil {
		panic(err)
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			panic(err)
		}
		io.Copy(os.Stdout, conn)
	}
}
